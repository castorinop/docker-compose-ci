FROM docker as docker-compose-ci
RUN  apk add --no-cache docker-cli-compose
RUN docker-compose --version

RUN apk add --no-cache kubectl 
RUN kubectl version --client

FROM docker-compose-ci as ci-toolbox
RUN ARCH=`uname -m` wget https://github.com/mikefarah/yq/releases/latest/download/yq_linux_amd64 -O /usr/bin/yq \
 && chmod +x /usr/bin/yq \
 && yq --version 
